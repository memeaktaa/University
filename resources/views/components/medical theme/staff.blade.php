@extends('layouts.app')
@section('content')

<!-- Page Title
============================================= -->
<section id="page-title" class="nobg">

	<div class="container clearfix">
		<h1>Doctors</h1>
		<span>A Short Page Title Tagline</span>
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active">Doctors</li>
		</ol>
	</div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

	<div class="content-wrap">

		<div class="container clearfix">

			<div class="col_one_fourth">

				<div class="team">
					<div class="team-image">
						<img src="images/doctors/1.jpg" alt="Dr. John Doe">
					</div>
					<div class="team-desc">
						<div class="team-title">
							<a href="#"><h4>Dr. John Doe</h4></a>
							<a href="#"><span>Cardiologist</span></a>
						</div>
					</div>
				</div>

			</div>

			<div class="col_one_fourth">
				<div class="team">
					<div class="team-image">
						<img src="images/doctors/2.jpg" alt="Dr. John Doe">
					</div>
					<div class="team-desc">
						<div class="team-title">
							<a href="#"><h4>Dr. Bryan Mcguire</h4></a>
							<a href="#"><span>Orthopedist</span></a>
						</div>
					</div>
				</div>
			</div>

			<div class="col_one_fourth">
				<div class="team">
					<div class="team-image">
						<img src="images/doctors/3.jpg" alt="Dr. John Doe">
					</div>
					<div class="team-desc">
						<div class="team-title">
							<a href="#"><h4>Dr. Mary Jane</h4></a>
							<a href="#"><span>Neurologist</span></a>
						</div>
					</div>
				</div>
			</div>

			<div class="col_one_fourth col_last">
				<div class="team">
					<div class="team-image">
						<img src="images/doctors/4.jpg" alt="Dr. John Doe">
					</div>
					<div class="team-desc">
						<div class="team-title">
							<a href="#"><h4>Dr. Silvia Bush</h4></a>
							<a href="#"><span>Dentist</span></a>
						</div>
					</div>
				</div>
			</div>

			<div class="clear"></div>

			<div class="col_one_fourth">
				<div class="team">
					<div class="team-image">
						<img src="images/doctors/6.jpg" alt="Dr. John Doe">
					</div>
					<div class="team-desc">
						<div class="team-title">
							<a href="#"><h4>Dr. Hugh Baldwin</h4></a>
							<a href="#"><span>Cardiologist</span></a>
						</div>
					</div>
				</div>
			</div>

			<div class="col_one_fourth">
				<div class="team">
					<div class="team-image">
						<img src="images/doctors/7.jpg" alt="Dr. John Doe">
					</div>
					<div class="team-desc">
						<div class="team-title">
							<a href="#"><h4>Dr. Erika Todd</h4></a>
							<a href="#"><span>Neurologist</span></a>
						</div>
					</div>
				</div>
			</div>

			<div class="col_one_fourth">
				<div class="team">
					<div class="team-image">
						<img src="images/doctors/8.jpg" alt="Dr. John Doe">
					</div>
					<div class="team-desc">
						<div class="team-title">
							<a href="#"><h4>Dr. Randy Adams</h4></a>
							<a href="#"><span>Dentist</span></a>
						</div>
					</div>
				</div>
			</div>

			<div class="col_one_fourth col_last">
				<div class="team">
					<div class="team-image">
						<img src="images/doctors/9.jpg" alt="Dr. John Doe">
					</div>
					<div class="team-desc">
						<div class="team-title">
							<a href="#"><h4>Dr. Alan Freeman</h4></a>
							<a href="#"><span>Eye Specialist</span></a>
						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="promo promo-flat promo-dark promo-full uppercase footer-stick">
			<div class="container clearfix">
				<h3 style="letter-spacing: 2px;">Start Planning your New Dream Home with us</h3>
				<span class="nott">We strive to provide Our Customers with Top Notch Support to make their Theme</span>
				<a href="#" class="button button-large button-border button-rounded button-light button-white">Contact Us</a>
			</div>
		</div>

	</div>

</section><!-- #content end -->
@endsection