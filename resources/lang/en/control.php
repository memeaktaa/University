<?php
/**
 * Created by PhpStorm.
 * User: Eng.Mohammed
 * Date: 9/8/2017
 * Time: 1:40 PM
 */
return [
    'tables' => 'Tables',
    'settings' => 'Settings',
    'public' => 'Public',
    'statistics' => 'Statistics',
];