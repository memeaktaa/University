<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'account'=>'Account',
    'contact'=>'Contact',
    'extra_information'=>'Extra Information',
    'profile'=>'Profile',
    'by_id'=>'By University ID',
    'by_email'=>'By Email'

];
