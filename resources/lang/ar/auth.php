<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'بيانات الاعتماد هذه غير متطابقة مع البيانات المسجلة لدينا.',
    'throttle' => 'عدد كبير جدا من محاولات الدخول. يرجى المحاولة مرة أخرى بعد :seconds ثانية.',
    'account'=>'الحساب',
    'contact'=>'الاتصال',
    'extra_information'=>'معلومات إضافية',
    'profile'=>'الملف الشخصي',
    'by_id'=>'باستخدام الرقم الجامعي',
    'by_email'=>'باستخدام الإيميل'

];
