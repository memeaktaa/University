@yield('styles')

@yield('content')

@yield('content-js')
<div class="wrapper">
    @yield('modal')
</div>

@yield('js')