<?php

namespace Modules\General\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Modules\General\Entities\Style
 *
 * @property int $style_id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\General\Entities\Style whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\General\Entities\Style whereStyleId($value)
 * @mixin \Eloquent
 */
class Style extends Model
{
    //
}
