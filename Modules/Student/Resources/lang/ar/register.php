<?php
/**
 * Created by PhpStorm.
 * User: Eng.Mohammad
 * Date: 10/27/2017
 * Time: 4:45 PM
 */
return [
    'register' => 'تسجيل المفاضلة',
    'name'=>'الاسم',
    'count'=>'عدد عناصر الإدخال',
    'total_baccalurate'=>'مجموع البكالوريا',
    'create'=>'أنشئ',
    'success'=>'تم الحفظ بنجاح',
    'failed'=>'المجموع الذي أدخلته لا يتوافق مع مجموع العلامات'
];